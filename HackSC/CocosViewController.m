//
//  CocosViewController.m
//  HackSC
//
//  Created by Patrick Bradshaw on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "CocosViewController.h"
#import "GameplayScene.h"
#import "FirebaseSingleton.h"

@implementation CocosViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CCDirector *director = [CCDirector sharedDirector];
    
    if([director isViewLoaded] == NO)
    {
        // Create the OpenGL view that Cocos2D will render to.
        CCGLView *glView = [CCGLView viewWithFrame:[[[UIApplication sharedApplication] keyWindow] bounds]
                                       pixelFormat:kEAGLColorFormatRGB565
                                       depthFormat:0
                                preserveBackbuffer:NO
                                        sharegroup:nil
                                     multiSampling:NO
                                   numberOfSamples:0];
        
        // Assign the view to the director.
        director.view = glView;
        
        // Initialize other director settings.
        [director setAnimationInterval:1.0f/60.0f];
        //[director enableRetinaDisplay:YES];
    }
    
    // Set the view controller as the director's delegate, so we can respond to certain events.
    director.delegate = self;
    
    // Add the director as a child view controller of this view controller.
    [self addChildViewController:director];
    
    // Add the director's OpenGL view as a subview so we can see it.
    [self.view addSubview:director.view];
    [self.view sendSubviewToBack:director.view];
    
    // Finish up our view controller containment responsibilities.
    [director didMoveToParentViewController:self];
    
}

-(void)setGameNum:(int)num
{
    CCDirector *director = [CCDirector sharedDirector];
    // Run whatever scene we'd like to run here.
    if(director.runningScene)
    {
        [director replaceScene:[[GameplayScene alloc]initWithGameNum:num ViewController:self]];
    }
    else
    {
        [director runWithScene:[[GameplayScene alloc]initWithGameNum:num ViewController:self]];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [[CCDirector sharedDirector] setDelegate:nil];
}

-(void)goBackToQC
{
    self.navigationItem.title = @"GameSelect";
    
    // Transition to the Navigation Controller
    [[CCDirector sharedDirector] end];
    [((UINavigationController*)self.parentViewController) popToRootViewControllerAnimated:YES];
    
    /*
    
    UIViewController *controller = [[FirebaseSingleton GetFirebase] getNavController];
    
    [UIView
     transitionFromView:self.view
     toView:controller.view
     duration:0.25f
     options:UIViewAnimationOptionTransitionCrossDissolve
     completion:^(BOOL finished) {
         //[currentChild removeFromParentViewController];
         [self.parentViewController addChildViewController:controller];
         //[controller.navigationItem setHidesBackButton:YES];
     }];*/
}

@end
