//
//  GameRequestsViewController.m
//  HackSC
//
//  Created by Patrick Bradshaw on 11/9/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "GameRequestsViewController.h"
#import <Firebase/Firebase.h>
#import "FirebaseSingleton.h"
#import "AltLobbyTableViewController.h"

@implementation GameRequestsViewController
-(id)initWithCoder:(NSCoder*)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    invites = [NSMutableArray array];
    names = [NSMutableArray array];
    
    //[self.tableView reloadData];
    [self beginListening];
    
    return self;
}

-(void)beginListening
{
    NSString *userID = [[FirebaseSingleton GetFirebase] getID];
    Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/GameRequests", userID]];
    [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        // Start fresh
        [invites removeAllObjects];
        [names removeAllObjects];
        
        // Cycle through all participants and add them
        NSArray *gamesList = [NSMutableArray arrayWithArray:[snapshot.value componentsSeparatedByString:@","]];

        [invites addObjectsFromArray:gamesList];
        
        if([invites count] > 0 && [[invites objectAtIndex:0] isEqualToString:@""])
        {
            [invites removeAllObjects];
            [names removeAllObjects];
            return;
        }
        
        for(NSString *strNum in invites){
            Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%@/Leader", strNum]];
            [f observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                NSString *leaderID = snapshot.value;
                Firebase *f2 = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/UserName", leaderID]];
                [f2 observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *child) {
                    [names addObject:child.value];
                    [self.tableView reloadData];
                }];
            }];
        }
        
        // End by reloading the table
        //[self.tableView reloadData];
    }];
}

// Table View
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //Fix this later - should be the size of the friends list
    return [invites count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString*Cellidentifier= [NSString stringWithFormat:@"%ld_%ld", (long)indexPath.section, (long)indexPath.row];
    UITableViewCell*cell;// =[tableView dequeueReusableCellWithIdentifier:Cellidentifier];
    
    if(indexPath.row <= [invites count]){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cellidentifier];
        cell.backgroundColor = [UIColor grayColor];
        if(indexPath.row <= [names count])
        {
            cell.textLabel.text = [names objectAtIndex:indexPath.row];
        }
    }
    return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Find the game number
    int gameNum = [[invites objectAtIndex:indexPath.row] intValue];
	
	//Remove All other invites
	NSString *userID = [[FirebaseSingleton GetFirebase] getID];
	Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/GameRequests", userID]];
	[f setValue:@""];

    
    //Transition to friends table controller
    self.navigationItem.title = @"Lobby";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AltLobbyTableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"AltLobbyVC"];
    [controller setGameNum:gameNum];
        
    [self.navigationController pushViewController:controller animated:YES];
}
@end
