//
//  AltLobbyTableViewController.m
//  HackSC
//
//  Created by Seyun Kim on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "AltLobbyTableViewController.h"
#import "FriendsTableViewController.h"
#import <Firebase/Firebase.h>
#import "FirebaseSingleton.h"
#import "CocosViewController.h"

@implementation AltLobbyTableViewController


-(id)initWithCoder:(NSCoder*)aDecoder{
	self = [super initWithCoder:aDecoder];
	
	friends = [NSMutableArray array];
	names = [NSMutableArray array];
	
	return self;
}

-(void)setGameNum:(int)num
{
    gameNum = num;
    
    Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/", gameNum]];
    [f observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        int numPart = 0;
        int numReady = 0;
        for(FDataSnapshot *child in [snapshot children]){
            if([child.key isEqualToString:@"NumParticipants"]){
                numPart = [child.value intValue];
            }
            else if([child.key isEqualToString:@"NumReady"]){
                numReady = [child.value intValue];
            }
            else if([child.key isEqualToString:@"GameState"]){
                if(![child.value isEqualToString:@"LOBBY"]){
                    // The game is not in the lobby state, you can't join
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    
                    [f removeAllObservers];
                    
                    return;
                }
            }
        }
        numPart++;
        numReady++;
        [[f childByAppendingPath:@"NumParticipants"] setValue:[NSNumber numberWithInt:numPart]];
        [[f childByAppendingPath:@"NumReady"] setValue:[NSNumber numberWithInt:numReady]];
        
        // Now lets add the user as a participant
        NSString *userID = [[FirebaseSingleton GetFirebase] getID];
        NSString *userName = [[FirebaseSingleton GetFirebase] getName];
        [[[f childByAppendingPath:@"Participants"] childByAppendingPath:userID] setValue:userName];
        
        [self beginListening];
    }];
    
    //[self.tableView reloadData];
    
}

-(void)beginListening
{
    
    Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/", gameNum]];
    [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        int numPart = 0;
        int numReady = 0;
        for(FDataSnapshot *child in [snapshot children]){
            if([child.key isEqualToString:@"Participants"]){
                // Start fresh
                [friends removeAllObjects];
                [names removeAllObjects];
        
                // Cycle through all participants and add them
                for(FDataSnapshot *childchild in [child children]){
                    NSString *userID = [NSString stringWithFormat:@"%@", childchild.key];
                    NSString *name = [NSString stringWithFormat:@"%@",childchild.value];
                    [friends addObject:userID];
                    [names addObject:name];
                }
        
                // End by reloading the table
                [self.tableView reloadData];
            }
            else if([child.key isEqualToString:@"NumParticipants"]){
                numPart = [child.value intValue];
            }
            else if([child.key isEqualToString:@"NumReady"]){
                numReady = [child.value intValue];
            }
        }
        if(numPart == numReady){
            // Transition to the game scene
            [f removeAllObservers];
            self.navigationItem.title = @"Game";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CocosViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CocosViewCtrl"];
            [controller setGameNum:gameNum];
            
            
            [self.navigationController pushViewController:controller animated:YES];
        }
        else {
            [self.tableView reloadData];
        }
    }];
}

// Table View
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//Fix this later - should be the size of the friends list
	return [friends count];
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString*Cellidentifier= [NSString stringWithFormat:@"%ld_%ld", (long)indexPath.section, (long)indexPath.row];
	UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:Cellidentifier];
	
	if(true){
		cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cellidentifier];
		cell.backgroundColor = [UIColor grayColor];
			cell.textLabel.text = [names objectAtIndex:indexPath.row];
	}
	return cell;
}

@end
