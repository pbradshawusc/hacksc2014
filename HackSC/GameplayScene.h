//
//  GameplayScene.h
//  HackSC
//
//  Created by Patrick Bradshaw on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "cocos2d.h"
#import "CocosViewController.h"

@interface GameplayScene : CCLayer <CCTouchAllAtOnceDelegate>
{
    CCSprite *potato;
    CGFloat clickedXPos, clickedYPos;
    NSString *potatoHolder;
    int gameNum, remainingTime;
    NSMutableArray *participants;
    NSTimer *timer;
    BOOL gameOver;
    CocosViewController *VC;
    NSString *loserName;
    CCLabelTTF *roasted;
}
-(id)initWithGameNum:(int)num ViewController:(CocosViewController*)c;
-(void)tick:(NSTimer*)t;
-(void)leave:(NSTimer*)t;
@end
