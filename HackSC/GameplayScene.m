//
//  GameplayScene.m
//  HackSC
//
//  Created by Patrick Bradshaw on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "GameplayScene.h"
#import <Firebase/Firebase.h>
#import "FirebaseSingleton.h"

@implementation GameplayScene
+(CCScene *) sceneWithGameNum:(int)num ViewController:(CocosViewController*)c
{
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];
    
    
    // add layer as a child to scene
    GameplayScene *layer = [[GameplayScene alloc] initWithGameNum:num ViewController:c];
    [scene addChild: layer z:0];
    
    
    // return the scene
    return scene;
}
-(id)initWithGameNum:(int)num ViewController:(CocosViewController *)c
{
    self = [super init];
    [self setTouchEnabled:YES];
    
    VC = c;
    timer = nil;
    gameOver = NO;
    gameNum = num;
    participants = nil;
    Firebase *fGame = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/",gameNum]];
    
    // Read data and react to changes
    [fGame observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        NSLog(@"%@ -> %@", snapshot.key, snapshot.value);
        for(FDataSnapshot *child in [snapshot children]){
            if([child.key isEqualToString:@"Potato"]){
                potatoHolder = child.value;
            }
            else if([child.key isEqualToString:@"RemainingTime"]){
                remainingTime = [child.value intValue];
            }
            else if([child.key isEqualToString:@"Participants"]){
                if(participants == nil){
                    participants = [NSMutableArray array];
                    for(FDataSnapshot*childchild in [child children]){
                        [participants addObject:childchild.key];
                    }
                }
            }
            else if([child.key isEqualToString:@"Loser"]){
                if(![child.value isEqualToString:@"none"]){
                    // Leave the game
                    gameOver = YES;
                    
                    loserName = child.value;
                    [self showLoser];
                    
                    [fGame removeAllObservers];
                    
                    return;
                }
            }
            else if([child.key isEqualToString:@"Leader"]){
                // Check to see if you are the leader and if the timer is still nil & the game isn't over
                if([child.value isEqualToString:[[FirebaseSingleton GetFirebase] getID]] && timer == nil && !gameOver){
                    timer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                             target:self
                                                           selector:@selector(tick:)
                                                           userInfo:nil
                                                            repeats:YES];
                }
            }
        }
        [self reload];
    }];
    
    return self;
}

// Functional Methods
-(void)reload
{
    [self removeAllChildrenWithCleanup:YES];
    
    // Query the size of the window
    CGSize size = [CCDirector sharedDirector].winSize;
    
    // Add the background
    CCSprite *wood = [CCSprite spriteWithFile:@"Wood.jpg"];
    wood.scaleX = (size.width)/wood.contentSize.width;
    wood.scaleY = (size.height)/wood.contentSize.height;
    wood.position = CGPointMake(size.width/2, size.height/2);
    [self addChild:wood];
    CCSprite *background = [CCSprite spriteWithFile:@"Glove.png"];
    background.scaleX = (size.width)/background.contentSize.width;
    background.scaleY = (size.height*2/3)/background.contentSize.height;
    background.position = CGPointMake(size.width/2, size.height*1/3);
    [self addChild:background];
    
    FirebaseSingleton* sing = [FirebaseSingleton GetFirebase];
    if([potatoHolder isEqualToString:[sing getID]]){
        // Bring in the potato!
        if(remainingTime <= 0){
            potato = [CCSprite spriteWithFile:@"VBurntPotato.png"];
            [self loseGame];
        }
        else if(remainingTime < 3){
            potato = [CCSprite spriteWithFile:@"PotatoTime6.png"];
        }
        else if(remainingTime < 6){
            potato = [CCSprite spriteWithFile:@"PotatoTime5.png"];
        }
        else if(remainingTime < 10){
            potato = [CCSprite spriteWithFile:@"PotatoTime4.png"];
        }
        else if(remainingTime < 15){
            potato = [CCSprite spriteWithFile:@"PotatoTime3.png"];
        }
        else if(remainingTime < 23){
            potato = [CCSprite spriteWithFile:@"PotatoTime2.png"];
        }
        else if(remainingTime < 30){
            potato = [CCSprite spriteWithFile:@"PotatoTime1.png"];
        }
        else{
            potato = [CCSprite spriteWithFile:@"InitialPotatoDesign.png"];
        }
        potato.scaleX = (size.width*2/3)/potato.contentSize.width;
        potato.scaleY = (size.height*2/3)/potato.contentSize.height;
        potato.position = CGPointMake(size.width/2, size.height/2);
        [self addChild:potato];
    }
}
-(void)sendPotato
{
    if(gameOver){
        return;
    }
    // First pick a target, but not you!
    FirebaseSingleton* sing = [FirebaseSingleton GetFirebase];
    NSString *target = nil;
    while(target == nil || [target isEqualToString:[sing getID]]){
        target = [participants objectAtIndex:(rand()%[participants count])];
    }
    
    // Set the new data on Firebase
    Firebase *fGame = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/Potato",gameNum]];
    [fGame setValue:target];
}
-(void)loseGame
{
    // Set the new data on Firebase
    FirebaseSingleton* sing = [FirebaseSingleton GetFirebase];
    Firebase *fGame = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/Loser",gameNum]];
    [fGame setValue:[sing getName]];
}
-(void)tick:(NSTimer*)t
{
    // Set the new data on Firebase
    Firebase *fGame = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/RemainingTime",gameNum]];
    [fGame setValue:[NSNumber numberWithInt:(remainingTime-2)]];
    if((remainingTime-2) <= 0)
    {
        [timer invalidate];
        timer = nil;
    }
}
-(void)showLoser
{
    [self removeAllChildrenWithCleanup:YES];
    // Add the background
    CGSize size = [CCDirector sharedDirector].winSize;
    CCSprite *wood = [CCSprite spriteWithFile:@"Wood.jpg"];
    wood.scaleX = (size.width)/wood.contentSize.width;
    wood.scaleY = (size.height)/wood.contentSize.height;
    wood.position = CGPointMake(size.width/2, size.height/2);
    [self addChild:wood];
    if([loserName isEqualToString:[[FirebaseSingleton GetFirebase] getName]]){
        potato = [CCSprite spriteWithFile:@"VBurntPotato.png"];
    }
    else {
        potato = [CCSprite spriteWithFile:@"InitialPotatoDesign.png"];
    }
    potato.scaleX = (size.width*2/3)/potato.contentSize.width;
    potato.scaleY = (size.height*2/3)/potato.contentSize.height;
    potato.position = CGPointMake(size.width/2, size.height/3);
    [self addChild:potato];
    
    
    
    // Add a label to show who was roasted
    roasted = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@\nGot Roasted!", loserName] fontName:@"Helvetica" fontSize:36.0f];
    roasted.position = CGPointMake(size.width/2, size.height*2/3);
    [self addChild:roasted];
    
    
    if(timer){
        [timer invalidate];
        timer = nil;
    }

    timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                             target:self
                                           selector:@selector(leave:)
                                           userInfo:nil
                                            repeats:NO];
}
-(void)leave:(NSTimer*)t
{
    [self removeAllChildrenWithCleanup:YES];
    [VC goBackToQC];
    //[self resetData];
}
-(void)resetData
{
    [self removeAllChildrenWithCleanup:YES];
    
    if(potato){
        [potato removeFromParentAndCleanup:YES];
    }
    potato = nil;
    clickedXPos = 0;
    clickedYPos = 0;
    potatoHolder = nil;
    gameNum = 0;
    remainingTime = 0;
    if(participants){
        [participants removeAllObjects];
    }
    if(timer){
        [timer invalidate];
    }
    if(roasted){
        [roasted removeFromParentAndCleanup:YES];
    }
    roasted = nil;
    timer = nil;
    gameOver = NO;
    VC = nil;
    loserName = nil;
    
    [self cleanup];
}

// Touch Recognition
-(void)registerWithTouchDispatcher
{
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event{
    CGPoint location = [self convertTouchToNodeSpace: touch];
    clickedXPos = location.x;
    clickedYPos = location.y;
    return YES;
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint location = [self convertTouchToNodeSpace: touch];
    
    if(abs(clickedXPos-location.x)>50 || abs(clickedYPos-location.y)>50)
    {
        // Register this as a swipe - we can even use the ending position data to determine the new potato recipient
        
        NSLog(@"Swipe Detected");
        [self sendPotato];
        
        return;
    }
}

@end
