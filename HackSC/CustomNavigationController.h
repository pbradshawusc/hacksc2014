//
//  CustomNavigationController.h
//  HackSC
//
//  Created by Patrick Bradshaw on 11/11/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController
{
    
}
-(void) makeMePretty;
-(void)moveToGameRequestList;
@end
