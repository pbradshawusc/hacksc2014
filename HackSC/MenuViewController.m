//
//  MenuViewController.m
//  HackSC
//
//  Created by Patrick Bradshaw on 11/7/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuViewController.h"
#import "FirebaseSingleton.h"
#import <Firebase/Firebase.h>

@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    loggedIn = false;
    FBLoginView *loginView =
    [[FBLoginView alloc] initWithReadPermissions:
     @[@"public_profile", @"user_friends"]];
    loginView.delegate = self;
    // Align the button in the center horizontally
    loginView.frame = CGRectOffset(loginView.frame, (self.view.center.x - (loginView.frame.size.width / 2)), self.view.center.y);
    [self.view addSubview:loginView];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (void) loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    /*self.navigationItem.title = @" ";
    ENCartViewController *cartViewController = [[ENCartViewController alloc] initWithCart:_itemsInCart andRestaurant:_selectedRestaurant];
    [self.navigationController pushViewController: cartViewController animated:YES];
*/
}

// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    if(loggedIn){
        return;
    }
    loggedIn = true;
    
    
    // Set Up the Firebase link and check to see if this user exists
    Firebase *fUsers = [[Firebase alloc] initWithUrl:@"https://hacksc2014.firebaseio.com/Users/"];
    [fUsers observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
     {
         for(FDataSnapshot* child in [snapshot children])
         {
             if([child.key isEqualToString:[NSString stringWithFormat:@"%@",user.id]]){
                // Logged in, transition to the next stage
                 FirebaseSingleton *data = [FirebaseSingleton GetFirebase];
                 [data setUserName:[NSString stringWithFormat:@"%@", user.id]];
                 [data setActualName:[NSString stringWithFormat:@"%@", user.name]];
                 for(FDataSnapshot* childchild in [child children]){
                     if([childchild.key isEqualToString:@"GameRequests"]){
                         [data addGames:childchild.value];
                         break;
                     }
                 }
                 // Transition to the Navigation Controller
                 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                 UINavigationController *controller = (UINavigationController*)[storyboard
                                                                                instantiateViewControllerWithIdentifier: @"MainNavigationController"];
                 [UIView
                  transitionFromView:self.view
                  toView:controller.view
                  duration:0.25f
                  options:UIViewAnimationOptionTransitionCrossDissolve
                  completion:^(BOOL finished) {
                      //[currentChild removeFromParentViewController];
                      [self addChildViewController:controller];
                  }];
                 return;
             }
         }
         // If we reach here, then the user is new, so we need to create one
         Firebase *fNew = [fUsers childByAppendingPath:[NSString stringWithFormat:@"%@", user.id]];
         [fNew setValue:@{@"UserId" : [NSString stringWithFormat:@"%@", user.id], @"UserName": [NSString stringWithFormat:@"%@",user.name], @"GameRequests" : @""}];
         // Transition to the Navigation Controller
         UIViewController *controller = [[FirebaseSingleton GetFirebase] getNavController];
         
         [UIView
          transitionFromView:self.view
          toView:controller.view
          duration:0.25f
          options:UIViewAnimationOptionTransitionCrossDissolve
          completion:^(BOOL finished) {
              //[currentChild removeFromParentViewController];
              [self addChildViewController:controller];
          }];
     }];
}

// Handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures that happen outside of the app
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

@end