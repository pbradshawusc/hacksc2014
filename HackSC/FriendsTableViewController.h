//
//  FriendsTableViewController.h
//  HackSC
//
//  Created by Seyun Kim on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>//
#import "LobbyTableViewController.h"

@interface FriendsTableViewController : UITableViewController {
	
	__strong NSMutableArray *friends; // to get facebook friends
	__strong NSMutableArray *names;
	LobbyTableViewController *lobby;
    int gameNum;
}

- (IBAction)pickFriendsButtonClick:(id)sender;
-(void)addFriend:(NSString*)friend Name:(NSString*)name;
-(void)setLobby:(LobbyTableViewController*)lob;
-(void)setGameNum:(int)num;

@end
