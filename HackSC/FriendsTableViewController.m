//
//  FriendsTableViewController.m
//  HackSC
//
//  Created by Seyun Kim on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "FriendsTableViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Firebase/Firebase.h>

//@property (nonatomic, assign) id <FBLoginDelegate> fbDelegate;



@implementation FriendsTableViewController
-(id)initWithCoder:(NSCoder*)aDecoder{
	self = [super initWithCoder:aDecoder];
	
	friends = [NSMutableArray array];
	names = [NSMutableArray array];
	
	FBRequest* friendsRequest = [FBRequest requestForMyFriends];
	[friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
																								NSDictionary* result,
																								NSError *error) {
		NSArray* f = [result objectForKey:@"data"];
		NSLog(@"Found: %lu friends", (unsigned long)f.count);
		for (NSDictionary<FBGraphUser>* friend in f) {
			NSLog(@"I have a friend named %@ with id %@", friend.name, friend.id);
			[self addFriend:[NSString stringWithFormat:@"%@",friend.id] Name:[NSString stringWithFormat:@"%@",friend.name]];
		}
		[self.tableView reloadData];
	}];
	
	return self;
}

-(void)setLobby:(LobbyTableViewController*)lob
{
	lobby = lob;
}

-(void)setGameNum:(int)num
{
    gameNum = num;
}

-(void)addFriend:(NSString*)friend Name:(NSString*)name{
	[friends addObject:friend];
	[names addObject:name];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//Fix this later - should be the size of the friends list
	return [friends count];
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString*Cellidentifier= [NSString stringWithFormat:@"%ld_%ld", (long)indexPath.section, (long)indexPath.row];
	UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:Cellidentifier];
	
	if(cell==nil){
		cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cellidentifier];
		cell.backgroundColor = [UIColor redColor];
		cell.textLabel.text = [names objectAtIndex:indexPath.row];
	}
	return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *userID = [friends objectAtIndex:indexPath.row];
	//[lobby addFriend:[friends objectAtIndex:indexPath.row] Name:name];
    
    // Here is where we need to send an invitation
    Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/GameRequests", userID]];
    [f observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        NSString *gamesList = [NSString stringWithFormat:@"%@", snapshot.value];
        if([gamesList isEqualToString:@""]){
            gamesList = [NSString stringWithFormat:@"%i",gameNum];
        }
        else{
            gamesList = [gamesList stringByAppendingString:[NSString stringWithFormat:@",%i",gameNum]];
        }
        [f setValue:gamesList];
    }];
	
	[self.navigationController popViewControllerAnimated:YES];
}


@end
