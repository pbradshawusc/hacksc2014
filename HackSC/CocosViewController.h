//
//  CocosViewController.h
//  HackSC
//
//  Created by Patrick Bradshaw on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CocosViewController : UIViewController <CCDirectorDelegate>
-(void)goBackToQC;
-(void)setGameNum:(int)num;
@end
