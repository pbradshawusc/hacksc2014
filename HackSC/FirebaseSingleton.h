//
//  FirebaseSingleton.h
//  HackSC
//
//  Created by Patrick Bradshaw on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#ifndef HackSC_FirebaseSingleton_h
#define HackSC_FirebaseSingleton_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>
#import <FacebookSDK/FacebookSDK.h>

@interface FirebaseSingleton : NSObject
{
    Firebase *f;
    NSMutableArray *gamesList;
    NSString *userName;
    NSString *actualName;
    UINavigationController *rootController;
}
+(FirebaseSingleton *)GetFirebase;
-(void)addGames:(NSString *)list;
-(void)setUserName:(NSString *)name;
-(void)setActualName:(NSString*)name;
-(NSString *)getID;
-(NSString *)getName;
-(UINavigationController*)getNavController;
@end

#endif
