//
//  AltLobbyTableViewController.h
//  HackSC
//
//  Created by Seyun Kim on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AltLobbyTableViewController : UITableViewController{

__strong NSMutableArray *friends; // to get facebook friends
__strong NSMutableArray *names;
    
    //Important info
    int gameNum;
}
-(void)setGameNum:(int)num;

@end
