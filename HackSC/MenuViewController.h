//
//  MenuViewController.h
//  HackSC
//
//  Created by Patrick Bradshaw on 11/7/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#ifndef HackSC_MenuViewController_h
#define HackSC_MenuViewController_h

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface MenuViewController : UIViewController <FBLoginViewDelegate>
{
    bool loggedIn;
}
@end

#endif
