//
//  GameRequestsViewController.h
//  HackSC
//
//  Created by Patrick Bradshaw on 11/9/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameRequestsViewController : UITableViewController
{
    NSMutableArray *invites;
    NSMutableArray *names;
}
@end
