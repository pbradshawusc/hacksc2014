//
//  FirebaseSingleton.m
//  HackSC
//
//  Created by Patrick Bradshaw on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "FirebaseSingleton.h"

FirebaseSingleton *sing = nil;

@implementation FirebaseSingleton
+(FirebaseSingleton *)GetFirebase
{
    if(sing == nil){
        sing = [[FirebaseSingleton alloc] init];
    }
    return sing;
}
-(id)init
{
    self = [super init];
    
    //Set up the Firebase Link
    f = [[Firebase alloc] initWithUrl:@"https://hacksc2014.firebaseio.com"];
    
    return self;
}
-(void)addGames:(NSString *)list
{
    // Create a local copy of game invites
    NSString *gamesString = list;
    if(gamesList == nil){
        gamesList = [NSMutableArray arrayWithArray:[list componentsSeparatedByString:@","]];
    }
    else {
        NSArray *arr = [list componentsSeparatedByString:@","];
        for(NSString *str in arr){
            [gamesList addObject:str];
        }
        gamesString = @"";
        for(NSString *str in gamesList){
            gamesString = [gamesString stringByAppendingString:[str stringByAppendingString:@","]];
        }
        gamesString = [gamesString substringToIndex:[gamesString length]-1];
    }
    
    // Now fix the games on the server side
    NSLog(@"%@", [NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/",userName]);
    Firebase *fUser = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/GameRequests",userName]];
    [fUser setValue:gamesString];
}
-(void)setUserName:(NSString *)name
{
    userName = name;
}
-(NSString *)getID
{
    return userName;
}
-(void)setActualName:(NSString *)name
{
    actualName = name;
}
-(NSString *)getName
{
    return actualName;
}
-(UINavigationController*)getNavController
{
    if(rootController == nil){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        rootController = (UINavigationController*)[storyboard instantiateViewControllerWithIdentifier: @"MainNavigationController"];
    }
    return rootController;
}
@end