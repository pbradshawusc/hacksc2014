//
//  LobbyTableViewController.m
//  HackSC
//
//  Created by Seyun Kim on 11/8/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "LobbyTableViewController.h"
#import "FriendsTableViewController.h"
#import <Firebase/Firebase.h>
#import "FirebaseSingleton.h"
#import "CocosViewController.h"

@implementation LobbyTableViewController

-(id)initWithCoder:(NSCoder*)aDecoder{
	self = [super initWithCoder:aDecoder];
    
    [self.navigationItem setHidesBackButton:YES];
    
	
	friends = [NSMutableArray array];
	names = [NSMutableArray array];
	
	// Let's create the game now
    Firebase *fGame = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/NextNum"]];
    [fGame observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if([snapshot.key isEqualToString:@"NextNum"]){
            gameNum = [snapshot.value intValue];
            NSString *userID = [[FirebaseSingleton GetFirebase] getID];
            NSString *userName = [[FirebaseSingleton GetFirebase] getName];
            [fGame setValue:[NSNumber numberWithInt:(gameNum +1)]];
            Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/"]];
            Firebase *fActual = [f childByAppendingPath:[NSString stringWithFormat:@"%i", gameNum]];
            [fActual setValue:@{@"Leader" : userID, @"Loser": @"none", @"NumParticipants" : [NSNumber numberWithInt:1], @"NumReady" : [NSNumber numberWithInt:0], @"GameState" : @"LOBBY", @"Potato" : userID, @"RemainingTime" : [NSNumber numberWithInt:30]}];
            Firebase *fPart = [fActual childByAppendingPath:@"Participants"];
            [fPart setValue:@{userID : userName}];
            [self beginListening];
        }
    }];
	
	return self;
}

-(void)beginListening
{
    Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/Participants", gameNum]];
    [f observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        // Start fresh
        [friends removeAllObjects];
        [names removeAllObjects];
        
        // Cycle through all participants and add them
        for(FDataSnapshot *child in [snapshot children]){
            NSString *userID = [NSString stringWithFormat:@"%@", child.key];
            NSString *name = [NSString stringWithFormat:@"%@",child.value];
            [friends addObject:userID];
            [names addObject:name];
        }
        
        // End by reloading the table
        [self.tableView reloadData];
    }];
}

-(void)addFriend:(NSString*)friend Name:(NSString*)name{
	[friends addObject:friend];
	[names addObject:name];
	[self.tableView reloadData];
}

// Table View
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//Fix this later - should be the size of the friends list
	return [friends count]+2;
}
-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString*Cellidentifier= [NSString stringWithFormat:@"%ld_%ld", (long)indexPath.section, (long)indexPath.row];
	UITableViewCell*cell=[tableView dequeueReusableCellWithIdentifier:Cellidentifier];
	
	if(true){
		cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cellidentifier];
		cell.backgroundColor = [UIColor grayColor];
		if(indexPath.row == [friends count]){
			cell.textLabel.text = @"+";
		}
        else if(indexPath.row == [friends count] + 1){
            cell.textLabel.text = @"Start!";
        }
		else {
			cell.textLabel.text = [names objectAtIndex:indexPath.row];
		}
	}
	return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row == [friends count]){
		//Transition to friends table controller
        self.navigationItem.title = @"FriendsTable";
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		FriendsTableViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"FriendsVC"];
		[controller setLobby:self];
        [controller setGameNum:gameNum];
		
		[self.navigationController pushViewController:controller animated:YES];
	}
    else if(indexPath.row == [friends count]+1){
        if([friends count] > 1){
            //First change the game state
            Firebase *f = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/GameState", gameNum]];
            [f setValue:@"GAME"];
            
            //Now update the user count so that everyone else is triggered
            Firebase *f2 = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Games/%i/NumReady", gameNum]];
            [f2 observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                int numReady = [snapshot.value intValue];
                [f2 setValue:[NSNumber numberWithInt:(numReady + 1)]];
                
                //Transition into the game
                self.navigationItem.title = @"Game";
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                CocosViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"CocosViewCtrl"];
                [controller setGameNum:gameNum];
                
                [self.navigationController pushViewController:controller animated:YES];
            }];
        }
    }
}
@end
