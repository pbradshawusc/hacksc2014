//
//  CustomNavigationController.m
//  HackSC
//
//  Created by Patrick Bradshaw on 11/11/14.
//  Copyright (c) 2014 Patrick Bradshaw. All rights reserved.
//

#import "CustomNavigationController.h"
#import "MKNumberBadgeView.h"
#import <Firebase/Firebase.h>
#import "FirebaseSingleton.h"
#import "GameRequestsViewController.h"

@implementation CustomNavigationController
-(id)initWithCoder:(NSCoder*)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    // Theme the bar
    [self makeMePretty];
    
    // Set up the title so that we internally know that we're in the game select screen
    self.title = @"GameSelect";
    
    // Set up invites button
    for(UINavigationItem *bi in [self navigationBar].items)
    {
        // Begin observing Firebase
        Firebase *fUser = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"https://hacksc2014.firebaseio.com/Users/%@/GameRequests",[[FirebaseSingleton GetFirebase] getID]]];
        [fUser observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
            // First determine if we're on the right screen
            if([self.title isEqualToString:@"GameSelect"]){
                // Now figure out how many games we have
                int numOfGames = 0;
                if(![snapshot.value isEqualToString:@""]){
                    NSArray *gamesList = [NSMutableArray arrayWithArray:[snapshot.value componentsSeparatedByString:@","]];
                    numOfGames = (int)[gamesList count];
                }
                
                // Create the badge view
                MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(60, 00, 30,20)];
                number.value = numOfGames;
                
                // Create the cutsom button
                UIButton *invitesButton = [UIButton buttonWithType:UIButtonTypeCustom];
                invitesButton.frame = CGRectMake(0, 0, 70, 30);
                invitesButton.layer.cornerRadius = 8;
                [invitesButton setTitle:@"Button" forState:UIControlStateNormal];
                [invitesButton addTarget:self action:@selector(moveToGameRequestList) forControlEvents:UIControlEventTouchUpInside];
                //[invitesButton setBackgroundColor:[UIColor blueColor]];
                [invitesButton setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.2]];
                invitesButton.font = [UIFont systemFontOfSize:13];
                //[invitesButton setFont:[UIFont systemFontOfSize:13]];
                [invitesButton addSubview:number];
                
                // Now make this button the BarButtonItem
                UIBarButtonItem *newItem = [[UIBarButtonItem alloc] initWithCustomView:invitesButton];
                
                // Now we can actually add it to the bar
                NSArray *actionButtonItems = @[newItem];
                bi.rightBarButtonItems = actionButtonItems;
            }
        }];
        
        // We only actually need to do this once
        break;
    }
    
    
    //[[self navigationBar] setItems:actionButtonItems animated:YES];
    
    
    return self;
}
-(void)makeMePretty
{
    // Make the Navigation Bar look pretty
    [[self navigationBar] setBarTintColor:[UIColor colorWithRed:205/255.0f green:92/255.0f blue:92/255.0f alpha:1.0f]];
    [[self navigationBar] setTintColor:[UIColor whiteColor]];
    UIImage *titleImage = [UIImage imageNamed:@"icon.png"];
    UIImage *scaledImage =
    [UIImage imageWithCGImage:[titleImage CGImage]
                        scale:(titleImage.scale * 10.0)
                  orientation:(titleImage.imageOrientation)];
    [[self navigationBar] topItem].titleView = [[UIImageView alloc] initWithImage:scaledImage];
}
-(void)moveToGameRequestList
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GameRequestsViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"GameRequestsVC"];
    
    self.title = @"GameRequests";
    
    [self pushViewController:controller animated:YES];
}
@end
